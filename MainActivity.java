
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import cn.easyar.CameraDevice;
import cn.easyar.Engine;
import cn.easyar.ImageTracker;

public class MainActivity extends AppCompatActivity {

    private static final String key = "";
    private static final int REQUEST_CODE_PERMISTION = 210;
    private GLView glView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        
        // permissions
        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.CAMERA, Manifest.permission.INTERNET
        }, REQUEST_COE_PERMISTION);

        // initialization
        if (!Engine.initialize(this, key)){
            Toast.makeText(this, Engine.errorMessage(), Toast.LENGTH_SHORT).show();
            finish();
        }
        if (!CameraDevice.isAvailable()){
            Toast.makeText(this, "No camera found", Toast.LENGTH_SHORT).show();
            finish();
        }
        if (!ImageTracker.isAvailable()){
            Toast.makeText(this, "No image tracking available", Toast.LENGTH_SHORT).show();
            finish();
        }
        glView = new GLView(this);
        ((ViewGroup)findViewById(R.id.preview)).addView(glView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_PERMISTION){
            // todo check whether there is camera access
            for (int i = 0; i < permissions.length; i++){
                Log.v("permit", permissions[i] + " " +grantResults[i]);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (glView != null){
            glView.onPause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (glView != null){
            glView.onResume();
        }
    }
}